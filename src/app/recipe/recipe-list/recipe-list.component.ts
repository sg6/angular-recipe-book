import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.scss']
})
export class RecipeListComponent implements OnInit {
  recipes: Recipe[] = [
    new Recipe('My test recipe', 'My test description',
    'https://yupitsvegan.com/wp-content/uploads/2017/01/sticky-sesame-cauliflower-vegan-6.jpg'),
    new Recipe('This recipe', 'this description',
    'https://yupitsvegan.com/wp-content/uploads/2017/01/sticky-sesame-cauliflower-vegan-6.jpg'),
    new Recipe('Another recipe', 'Another description',
    'https://yupitsvegan.com/wp-content/uploads/2017/01/sticky-sesame-cauliflower-vegan-6.jpg'),
  ];
  @Output() recipeWasSelected = new EventEmitter<Recipe>();

  constructor() { }

  ngOnInit() {
  }

  onRecipeSeletected(recipe: Recipe) {
    this.recipeWasSelected.emit(recipe);
  }

}
