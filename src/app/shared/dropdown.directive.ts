import { Directive, Renderer2, OnInit, ElementRef, HostBinding, HostListener } from '@angular/core';

@Directive({
  selector: '[appDropdown]'
})
export class DropdownDirective implements OnInit {
  @HostBinding('class.active') isActive = false;
  private isOpen = false;

  constructor(private renderer: Renderer2, private elRef: ElementRef) { }

  @HostBinding('class.show') get opened() {
    return this.isOpen;
  }
  @HostListener('click') open() {
    const child = this.elRef.nativeElement.children[1];
    this.isOpen = !this.isOpen;
    console.log(this.elRef.nativeElement.children[1]);
    if (child.classList.contains('show') {
      child.classList.remove('show');
    } else {
      child.classList.add('show');
    }
  }

  ngOnInit() {
  }

}
