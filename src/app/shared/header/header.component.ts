import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  @Input() menuItem: String;
  @Output() menuItemChanged = new EventEmitter<String>();

  constructor() { }

  ngOnInit() {
  }

  showNavItem(menuItem: String) {
    this.menuItemChanged.emit(menuItem);
  }

}
