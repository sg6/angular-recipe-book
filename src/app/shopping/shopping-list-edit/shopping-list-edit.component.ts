import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Ingredient } from '../../shared/ingredient.model';

@Component({
  selector: 'app-shopping-list-edit',
  templateUrl: './shopping-list-edit.component.html',
  styleUrls: ['./shopping-list-edit.component.scss']
})
export class ShoppingListEditComponent implements OnInit {
  @Output() shoppinglistItemAdded = new EventEmitter<Ingredient>();

  constructor() { }

  ngOnInit() {
  }

  addItem(name, amount) {
    this.shoppinglistItemAdded.emit(new Ingredient(name, amount));
  }

}
